import RPi.GPIO as GPIO
import time

GPIO.cleanup()
GPIO.setmode(GPIO.BOARD)
GPIO.setup(12, GPIO.OUT)

while(1):
    print("turn off led")
    GPIO.output(12, 0)
    time.sleep(5)
    print("turn on led")
    GPIO.output(12, 1)
    time.sleep(5)