# Medusa

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Software License][ico-license]](LICENSE)
[![Build Status][ico-build-status]][link-travis]
[![Total Downloads][ico-downloads]][link-downloads]

[![codecov.io](https://codecov.io/github/mpociot/laravel-apidoc-generator/coverage.svg?branch=master)](https://codecov.io/github/mpociot/laravel-apidoc-generator?branch=master)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/mpociot/laravel-apidoc-generator/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/mpociot/laravel-apidoc-generator/?branch=master)
[![Build Status](https://travis-ci.org/mpociot/laravel-apidoc-generator.svg?branch=master)](https://travis-ci.org/mpociot/laravel-apidoc-generator)
[![StyleCI](https://styleci.io/repos/57999295/shield?style=flat)](https://styleci.io/repos/57999295)


*Leia isso em outra língua: [English](README.md), [Português (Brasileiro)](README.pt-br.md)*

## Índice

1. [ Descrição. ](#desc)
2. [ Usage tips. ](#usage)
3. [ Usage tips. ](#authors)
4. [ Change log. ](#changes)


## Introdução
PHP low-level client for Vespa. https://vespa.ai/

## Compatibilidade


<a name="authors"></a>
## Autores
For a list of all our amazing authors please see the contributors page:
https://github.com/icarojerry/medusa/graphs/contributors

    ## Log de alterações

    Consulte [CHANGELOG](CHANGELOG.md) para obter mais informações sobre o que mudou recentemente.


    ## Medusa Server
        ### Core

    Adicione uma nova linha ao arquivo de hosts da sua máquina.
    `127.0.0.1   local.medusa.com.br`

    `python manage.py migrate`
    `python manage.py createsuperuser --email admin@example.com --username admin`

### API

## Medusa Client
### Web
### Mobile


    ## Pre-Install Raspberry

        ### Requirements

        ### Update System Operational
        sudo apt-get update -y

        sudo apt-get install build-essential tk-dev libncurses5-dev libncursesw5-dev libreadline6-dev libdb5.3-dev libgdbm-dev libsqlite3-dev libssl-dev libbz2-dev libexpat1-dev liblzma-dev zlib1g-dev libffi-dev -y


        Install python 3.5
        1. wget https://www.python.org/ftp/python/3.5.2/Python-3.5.2.tar.xz
        2. tar -xvf Python-3.5.2.tar.xz
        3. cd Python-3.5.2
        4. ./configure
        5. make
        6. make altinstall

        sudo apt-get install python3-pip
        sudo python3 -m pip install --upgrade pip setuptools wheel

        wget https://bootstrap.pypa.io/get-pip.py
        sudo python get-pip.py
        pipenv install django
        pip install pipenv

        pipenv install
        pipenv check .


        ### Docker

        curl -sSL https://get.docker.com | sh
        sudo usermod -aG docker pi
        sudo systemctl enable docker
        systemctl start docker

        sudo apt install docker-compose



[ico-version]: https://img.shields.io/packagist/v/escavador/vespa-php.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-GPL3-brightgreen.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/escavador/vespa-php.svg?style=flat-square
[ico-build-status]: https://travis-ci.org/Escavador/vespa-php.svg?branch=master

[link-packagist]: https://packagist.org/packages/escavador/vespa-php
[link-downloads]: https://poser.pugx.org/escavador/vespa-php/downloads
[link-author]: https://github.com/escavador
[link-travis]: https://travis-ci.org/Escavador/vespa-php