# Medusa

*Read this in other languages: [English](README.md), [Português (Brasileiro)](README.pt-br.md)*

## Table of Contents

1. [ Description. ](#desc)
2. [ Usage tips. ](#usage)
3. [ Usage tips. ](#authors)
4. [ Change log. ](#changes)

<a name="authors"></a>
## Authors
For a list of all our amazing authors please see the contributors page:
https://github.com/icarojerry/medusa/graphs/contributors

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.