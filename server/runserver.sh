#!/bin/bash

cd /server
export PYTHONPATH=$PYTHONPATH:$PWD

python manage.py makemigrations
python manage.py migrate
python manage.py initadmin

python manage.py runserver  0.0.0.0:8001
