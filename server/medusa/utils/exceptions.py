class GenericException(Exception):
    def __init__(self):

        super().__init__(f"A generic exception has occurred")
