import configparser
import environ
import os

env = environ.Env()
env.read_env(env.str('ENV_PATH', '.env'))


__bumpver_cfg = configparser.ConfigParser()
__bumpver_cfg.read(
    os.path.join(os.path.dirname(os.path.realpath(__file__)), "../../.bumpversion.cfg")
)
__CURRENT_VERSION = __bumpver_cfg.get("bumpversion", "current_version")
del __bumpver_cfg

def version() -> str:
    return __CURRENT_VERSION


def get(section, option, default=None) -> str:
    return os.environ.get('_'.join([section.upper(), option.upper()]),
                           default)


def get_boolean(section, option, default=None) -> bool:
    return _get_converting_env_or_else(section, option,
                                       _convert_to_boolean,
                                       default)


def _get_converting_env_or_else(section, option, conv, default):
    return conv(os.environ.get('_'.join([section.upper(), option.upper()]),
                               default))


def _convert_to_boolean(value):
    """Return a boolean value translating from other types if necessary.
    """
    if value.lower() not in configparser.ConfigParser.BOOLEAN_STATES:
        raise ValueError(f'{value} is not a valid boolean value.')
    return configparser.ConfigParser.BOOLEAN_STATES[value.lower()]