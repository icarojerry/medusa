import logging
import os
import sys
import uuid
from logging.handlers import TimedRotatingFileHandler

import utils.env

__app_log_path = ""


def init(app_name, *, debug=False, aiohttp=False):

    debug = utils.env.is_debug(debug)

    global __app_log_path
    __app_log_path = os.path.join(
        os.path.dirname(os.path.realpath(__file__)), "..", "logs", app_name
    )
    filename = os.path.join(__app_log_path, app_name + ".log")

    os.makedirs(os.path.dirname(filename), exist_ok=True)

    formatter = logging.Formatter("%(levelname)s: <%(asctime)s>  %(message)s")

    rotating_handler = TimedRotatingFileHandler(
        filename, when="midnight", interval=40, utc=True
    )
    rotating_handler.setLevel(logging.INFO)
    rotating_handler.setFormatter(formatter)
    logging.getLogger().addHandler(rotating_handler)

    if debug:
        stdout_handler = logging.StreamHandler(sys.stdout)
        stdout_handler.setLevel(logging.DEBUG)
        stdout_handler.addFilter(lambda record: record.levelno <= logging.INFO)
        stdout_handler.setFormatter(formatter)

        stderr_handler = logging.StreamHandler(sys.stderr)
        stderr_handler.setLevel(logging.WARNING)
        stderr_handler.setFormatter(formatter)

        logging.getLogger().addHandler(stdout_handler)
        logging.getLogger().addHandler(stderr_handler)

    logging.getLogger().setLevel(logging.DEBUG if debug else logging.INFO)

    # Peewee produces a lot of spam under logging.DEBUG (all SQL queries are logged).
    logging.getLogger("peewee").setLevel(logging.INFO)


def dump(content):
    filepath = os.path.join(__app_log_path, "dump_" + uuid.uuid4().hex)
    logging.getLogger().info(f"Creating dump file at {filepath}")
    with open(filepath, "w") as f:
        f.write(content)
