from django.contrib import admin

from .models import Controlador, Aquario, Sensor, Atuador, Evento


@admin.register(Controlador)
class ControladorAdmin(admin.ModelAdmin):
    list_display = []


@admin.register(Aquario)
class AquarioAdmin(admin.ModelAdmin):
    list_display = []


@admin.register(Sensor)
class SensorAdmin(admin.ModelAdmin):
    list_display = []


@admin.register(Atuador)
class AtuadorAdmin(admin.ModelAdmin):
    list_display = []


@admin.register(Evento)
class EventoAdmin(admin.ModelAdmin):
    list_display = [
        'objeto_id',
        'objeto_classe',
        'horario',
        'fuso_horario',
        'dados',
    ]
