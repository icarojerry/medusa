from django.core.management.base import BaseCommand
from django.contrib.auth.models import User;
from medusa.utils import env

class Command(BaseCommand):

    def handle(self, *args, **options):
        username = env.get("django", "superuser_user")
        email = env.get("django", "superuser_email")
        password = env.get("django", "superuser_password")

        print(username)

        if not User.objects.filter(email = email):
            User.objects.create_superuser(username, email, password)
        else:
            print('O super usuário padrão já existe no banco.')
