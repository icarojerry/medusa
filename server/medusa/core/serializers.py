from rest_framework import serializers
from .models import Controlador, Aquario, Componente

class AquarioSerializer(serializers.ModelSerializer):

    class Meta:
        model = Aquario
        fields = (
            'id',
            'nome'
        )


class ControladorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Controlador
        fields = (
            'aquarios',
            'componentes'
        )


class ComponenteSerializer(serializers.ModelSerializer):

    class Meta:
        model = Componente
        fields = (
            'id',
            'nome'
        )