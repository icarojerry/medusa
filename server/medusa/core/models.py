from django.db import models
import jsonfield


class Base(models.Model):
    id = models.AutoField(primary_key=True)
    criado_em = models.DateTimeField(auto_now_add=True)
    ultima_atualizacao_em = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Usuario(Base):
    pass


class Controlador(Base):
    pass

class Aquario(Base):
    nome = models.CharField(max_length=500, blank=False, null=False, verbose_name="nome")
    controlador = models.ForeignKey(Controlador, related_name="aquarios", on_delete=models.CASCADE, verbose_name="controlador", blank=True)
    ip_camera = models.URLField(verbose_name="ip da câmera", blank=True)
    #proprietarios = models.ManyToManyField(Usuario, verbose_name="proprietários")

    class Meta:
        verbose_name = 'Aquário'
        verbose_name_plural = 'Aquários'
        ordering = ['nome']


class ComponenteComunicao(Base):
    pass


class Componente(Base):
    TIPO = (
        ('1', 'Digital'),
        ('2', 'Analógico'),
    )

    nome = models.CharField(max_length=500, blank=False, null=False, verbose_name="nome")
    status = models.BooleanField(default=False, blank=True)
    aquario = models.OneToOneField(Aquario, on_delete=models.CASCADE, verbose_name="aquario", blank=True)
    controlador = models.ForeignKey(Controlador, on_delete=models.CASCADE, related_name="%(class)s_componente", verbose_name="controlador")
    tipo = models.PositiveSmallIntegerField(default=1, choices=TIPO, verbose_name="tipo do sensor")
    #comunicao =

    @classmethod
    def ligado(self):
        return self.status == True

    @classmethod
    def ligar(self):
        self.status = True

    @classmethod
    def desligar(self):
        self.status = False

    class Meta:
        abstract = True


class Sensor(Componente):

    @classmethod
    def medir(self):
        self.status = False


class Atuador(Componente):
    pass

class Evento(Base):
    OBJETO_CLASSE = (
        ('1', f'{Usuario.__class__.__name__}'),
        ('2', f'{Aquario.__class__.__name__}'),
        ('3', f'{Componente.__class__.__name__}')
    )

    objeto_id = models.PositiveSmallIntegerField(default=0, verbose_name="Id do objeto do evento")
    objeto_classe = models.PositiveSmallIntegerField(default=1, choices=OBJETO_CLASSE, verbose_name="classe do objeto")
    horario = models.TimeField(blank=False, null=False, verbose_name="hora do evento")
    fuso_horario = models.TimeField(blank=False, null=False, verbose_name="fuso horário")
    dados = jsonfield.JSONField()

"""

"""
class Atividade(Base):
    ACAO = (
        ('1', 'Alerta'),
        ('2', 'Ligar'),
        ('3', 'Desligar'),
    )

    acao = models.PositiveSmallIntegerField(default=1, choices=ACAO, verbose_name="ação atividade")
    proxima = models.ForeignKey("self", on_delete=models.CASCADE, verbose_name="atividade", blank=True)

    def escolher(self):
        pass

    def executar(self):
        pass


class Alerta(Base):
    pass