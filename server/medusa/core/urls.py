from rest_framework.routers import SimpleRouter

from .views import AquarioViewSet, SensorViewSet, AtuadorViewSet

core_router_v1 = SimpleRouter()
core_router_v1.register('aquarios', AquarioViewSet)
core_router_v1.register('sensores', SensorViewSet)
core_router_v1.register('atuadores', AtuadorViewSet)
core_router_v1.register('controladores', AtuadorViewSet)

urlpatterns = [
#    # Rotas para configurar expurgo dos dados
]