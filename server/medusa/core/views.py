from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from .models import Aquario, Sensor, Atuador, Controlador
from .serializers import AquarioSerializer, ComponenteSerializer, ControladorSerializer

class AquarioViewSet(viewsets.ModelViewSet):
    queryset = Aquario.objects.all()
    serializer_class = AquarioSerializer


class ControladorViewSet(viewsets.ModelViewSet):
    queryset = Controlador.objects.all()
    serializer_class = ControladorSerializer

    @action(detail=True, methods=['get'])
    def componentes(self, request, pk=None):
        controlador = self.get_object()
        serializer = ComponenteSerializer(controlador.componentes.all(), many=True)
        return Response(serializer.data)

    @action(detail=True, methods=['get'])
    def componentes(self, request, pk=None):
        controlador = self.get_object()
        serializer = AquarioSerializer(controlador.aquarios.all(), many=True)
        return Response(serializer.data)


class SensorViewSet(viewsets.ModelViewSet):
    queryset = Sensor.objects.all()
    serializer_class = ComponenteSerializer


class AtuadorViewSet(viewsets.ModelViewSet):
    queryset = Atuador.objects.all()
    serializer_class = ComponenteSerializer