from medusa.settings.base import *

PRODUCTION = False

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS=['*']

#EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'