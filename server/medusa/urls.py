from django.contrib import admin
from django.urls import path, include

from medusa.core.urls import core_router_v1

#from django.views.generic import TemplateView

urlpatterns = [
    # path('', home),
    #path('/', heart_beat),

    path('api/v1/', include(core_router_v1.urls)),
    path('admin/', admin.site.urls),
    path('api/v1/auth/', include('rest_framework.urls')),
    #path('robots.txt', TemplateView.as_view(template_name="robots.txt")),
]
